* Vanilla-sky-theme
Vanilla-sky-theme is an Emacs theme based in colors found in the sky.

As a recommendation you can watch the "Vanilla sky" movie.

* How to install
- Copy the ~vanilla-sky-theme.el~ file to your
  ~custom-theme-directory~

- Copy the ~vanilla-sky-palette.el~ file to your ~load-path~
  directory.

- add the following code to your ~.emacs~ file:
#+BEGIN_SRC emacs-lisp
(load-theme 'vanilla-sky)
#+END_SRC
