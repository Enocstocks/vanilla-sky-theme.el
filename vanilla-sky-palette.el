;; 0 = font-lock-builtin-face
;; 1 = mode line foreground
;; 2 = highlight
;; 3 = modeline inactive & region dfg
;; 4 = latex titles
(defvar vanilla-sky-theme-palette '("#49b7fc" "#cbecff" "#313235" "#90acbc" "SandyBrown")
  "Vanilla-sky-theme palette")

(provide 'vanilla-sky-palette)
