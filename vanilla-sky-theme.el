(require 'vanilla-sky-palette)

(deftheme vanilla-sky)

(custom-theme-set-faces
 'vanilla-sky
 '(default ((t (:background "#17181b" :foreground "pink"))))
 `(font-lock-builtin-face ((t (:foreground ,(nth 0 vanilla-sky-theme-palette)))))
 '(font-lock-comment-face ((t (:foreground "#5A7387" :italic t))))
 '(font-lock-constant-face ((t (:foreground "#738FD7"))))
 '(font-lock-negation-char-face ((t (:foreground "#e84c58"))))
 '(font-lock-doc-face ((t (:foreground "#6b7d8c"))))
 `(font-lock-function-name-face ((t (:foreground ,(nth 0 vanilla-sky-theme-palette)))))
 `(font-lock-keyword-face ((t (:foreground ,(nth 0 vanilla-sky-theme-palette)))))
 '(font-lock-string-face ((t (:foreground "PaleTurquoise"))))
 '(font-lock-type-face ((t (:foreground "#bef76a"))))
 '(font-lock-variable-name-face ((t (:foreground "#49d17d"))))
 '(font-lock-warning-face ((t (:foreground "#f3c91f"))))
 '(font-lock-preprocessor-face ((t (:foreground "#719ffc"))))
 ;; modeline
 `(mode-line ((t (:bold nil :foreground ,(nth 1 vanilla-sky-theme-palette) :background "#3D5666"))))
 `(mode-line-inactive ((t (:bold nil :foreground ,(nth 3 vanilla-sky-theme-palette) :background "#1E1E1E"))))
 `(mode-line-buffer-id ((t (:bold nil :foreground ,(nth 1 vanilla-sky-theme-palette) :background nil))))
 `(mode-line-highlight ((t (:foreground ,(nth 0 vanilla-sky-theme-palette) :box nil :weight normal))))
 '(mode-line-emphasis ((t (:foreground "#b0ccdc"))))
 `(highlight ((t (:background ,(nth 2 vanilla-sky-theme-palette)))))
 `(region ((t (:background "#2B4255" :distant-foreground ,(nth 3 vanilla-sky-theme-palette)  :extend nil))))
 ;; latex titles
 '(font-latex-sectioning-0-face ((t (:bold nil :foreground "yellow"))))
 `(font-latex-sectioning-1-face ((t (:bold nil :foreground "yellow"))))
 `(font-latex-sectioning-2-face ((t (:bold nil :foreground "yellow"))))
 `(font-latex-sectioning-3-face ((t (:bold nil :foreground "yellow"))))
 `(font-latex-sectioning-4-face ((t (:bold nil :foreground "yellow"))))
 `(font-latex-sectioning-5-face ((t (:bold nil :foreground "yellow"))))
 ;; cursor
 '(cursor ((t (:background "HotPink"))))
 ;; tab bar
 '(tab-bar ((t (:background "#181a26" :foreground "white" :bold t))))
 '(tab-bar-tab-inactive ((t (:background "#505882" :bold nil))))
 '(tab-bar ((t (:background "green" :bold t))))
 ;; error
 '(error ((t (:foreground "red" :bold t))))
 ;; eshell
 '(eshell-prompt ((t (:foreground "SlateBlue"))))
 )

(provide-theme 'vanilla-sky)
